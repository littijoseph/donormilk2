# Getting started

Get the code from:

`git clone https://gitlab.com/littijoseph/donormilk.git`

`cd donorMilkClient`

`npm install`

`ng build`

After successfull build, you should see a donormilk folder inside the dist folder.

**NOTE**
The project is built on mac os. There are chances for changes in the nginx configuration files for macos and linux around the locations specified within the conf file. Hence providing 2 conf files.

**MACOS**
For MacOS use nginx_mac.conf

if nginx not installed on mac?

`sudo brew nginx`
    
    After successfull install, replace the nginx.conf file in /etc/nginx/nginx.conf with the one provided after renaming.

(`sudo cp nginx.conf /usr/local/etc/nginx/nginx.conf`)

    Stop and start again nginx using:
    
(`sudo nginx -s stop`)

(`sudo nginx`)

(if the port is still in use:
     get PID using `sudo lsof -i:4200`
    Do `sudo kill -9 <PID>` and then do the restart
)


**LINUX**
For Linux use nginx_linux.conf

if nginx not installed on linux?

`apt-get install nginx`

    After successfull install, replace the nginx.conf file in /etc/nginx/nginx.conf with the one provided after renaming.

(`sudo cp nginx.conf /etc/nginx/nginx.conf`)

    Restart nginx using `/etc/init.d/nginx restart`
    
(if the port is still in use:
     get PID using `sudo netstat -tulpn`
    Do `sudo kill -2 <PID>` and then do the restart
)

Open appropriate nginx conf file in the repository, replace '<donormilk_proj_dist_path>' with this folder path


`sudo docker-compose up`

The application should be available at `http://localhost:4200`

Running `node subscriptionProxy.js` will start the event subscription

## Introduction

Project Name : Donor Milk

Team Members : Litti Tom

This project invloves sawtooth implementation of the MVP usecase under the project Donor Milk Transparency and Traceability (https://wiki.hyperledger.org/groups/healthcare/donor_milk_transparency_and_traceability) of The Hyperledger Healthcare Special Interest Group https://wiki.hyperledger.org/groups/healthcare/healthcare-sig


## Components
The donormilk transaction family contains two parts :

	I)Client Application ,written in Angular
	II)Transaction Processor ,written in Python

1. The client application has two parts:

* `angular application` : contains the client application that can make transactions to the validator through REST API
* `node subscription service` : A node service that subscribes to events and listens.


2. The Transaction Processor in python.

   TransactionProcessor is a generic class for communicating with a validator and routing transaction processing requests to a registered handler. 
   handler.py has the Transaction Processor class.

   
   The handler class is application-dependent and contains the business logic for a particular family of transactions. 
   handler.py has the handler class.

   The python transaction processor has the following files :
    
                a)handler.py  (transaction processor class)
                b)donormilk_payload.py(Payload class to do the basic validation, verification and accept the payload)
                c)donormilk_state.py(provides the functions to perform the state updates.)
                d)package.json
                e)Dockerfile
		

       
## Docker Usage
### Prerequisites
This example uses docker-compose and Docker containers. If you do not have these installed please follow the instructions here: https://docs.docker.com/install/


### Building Docker containers

Before starting  the project make sure the Docker service is up and running.

To start up the environment, perform the following tasks:

    a)Open a terminal window.
    b)Change your working directory to the same directory where you saved the Docker Compose file.
    c)Run the following command:


	$sudo docker-compose up --build

	The `docker-compose.yaml` file creates a genesis block, which contain initial Sawtooth settings, generates Sawtooth and client keys, 
	and starts the Validator, Settings TP, DonorMilk TP, and REST API.


To stop the validator and destroy the containers, type `^c` in the docker-compose window, wait for it to stop, then type

	$sudo docker-compose down

### How to add permissions?

2 types of permissioning
- Transactor permissioning
  - For limiting which all transactors can submit batches and transactions to the validator
- Validator key permissioning


#### Transactor Permissioning

2 methods of implementation:
- Off-chain permissioning
    - configured using validator.toml file
    - list cannot be changed while validator is running
    - enforced when batch received from client
- On-chain permissioning
  - uses identity namespace
  - transactions are checked when 
    - a batch is received from a client
    - a batch is received from a peer
    - a block is validated

##### Off-chain Transactor Permissioning

* Format of validator.toml:

    ```toml
    [permissions]
    ROLE = POLICY_NAME
    ```

*  Multiple roles can be defined, using one “ROLE” = “POLICY_NAME” entry per line. Default: none.
*  `ROLE`:
   *  examples:

        `default`,
        `transactor`,
        `transactor.transaction_signer`,
        `transactor.transaction_signer.{tp_name}`,
        `transactor.batch_signer`

* `POLICY_NAME`
  * Specify a policy file in policy_dir (by default /etc/sawtooth/).
  * Example
    * policy.donormilkPolicy - Policy file
    ```
    PERMIT_KEY 02c150d578975c41e08958f53fd14231b5847ef76208508c96271704138ea8cb76
    PERMIT_KEY 030e8e21bff245c515e8e5a508877a284fbc9ead9b0c00152cf82853334b1bea41
    PERMIT_KEY 02280b14a9fe4ad3ce8ed60424d21326bcf04ad2f1214ee480239464bd8f60f403
    DENY_KEY *
    ```

* Sample configuration 

    ```toml
        [permissions]
        "transactor" = "policy.donormilkPolicy"
        "transactor.transaction_signer" = "policy.donormilkPolicy"
    ```

##### On-Chain Transactor Permissioning

* Identity Namespace stores roles as key-value pairs, where the key is a role name and the value is a policy. For configuring on-chain roles, the signer of identity transactions needs to have their public key set in the Setting `sawtooth.identity.allowed_keys`. 
  * ```sawset proposal create sawtooth.identity.allowed_keys=02b2be336a6ada8f96881cd55fd848c10386d99d0a05e1778d2fc1c60c2783c2f4```
* Once signer key is stored in the setting, the `identity-tp` command can be used to set and update roles and policies.
    * To create a policy named policy_1 that permits all is given by: 
        `sawtooth identity policy create policy_1 "PERMIT_KEY *"`
    * To set the role for transactor to permit all:
        `sawtooth identity role create transactor policy_1`

#### Validator Key Permissioning
Validator network must be able to limit the nodes that are able to connect to it. Validator network permissioning roles use the following pattern:

`network[.SUB_ROLE] = POLICY_NAME`

where network is the name of the role to be used for validator network permissioning. POLICY_NAME refers to the name of a policy that is set in the Identity namespace.

To set the role for network to permit all following command is used:

`sawtooth identity role create network policy_1`

* Network Roles: If a validator receives a peer request from a node whose public key is not permitted by the policy, the message will be dropped, an AuthorizationViolation will be returned, and the connection will be closed.

