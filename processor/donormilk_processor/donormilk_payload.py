# Copyright 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------------

import json
import logging

from sawtooth_sdk.processor.exceptions import InvalidTransaction

LOGGER = logging.getLogger(__name__)


class DonorMilkPayload(object):

    def __init__(self, payload):
        try:
            data = json.loads(payload.decode('utf-8'))
        except ValueError:
            raise InvalidTransaction("Invalid payload serialization")

        action = data.get('action')
        asset = data.get('asset')
        owner = data.get('owner')

        if not action:
            raise InvalidTransaction('Action is required')
        if action not in ('donorCreate', 'holderCreate', 'createMilk', 'transferMilk', 'acceptMilk', 'rejectMilk'):
            raise InvalidTransaction('Invalid action: {}'.format(action))

        if not asset:
            raise InvalidTransaction('Asset is required')

        if action == 'donorCreate':
            self._donor = asset

        elif action == 'holderCreate':
            self._holder = asset

        elif action == 'createMilk':
            LOGGER.debug('donateMilk action: %s', asset)
            self._milk = asset

        elif action == 'transferMilk':
            self._transfer = asset
            LOGGER.debug('transferMilk action: %s', asset)

        elif action == 'acceptMilk':
            self._transfer = asset
            LOGGER.debug('acceptMilk action: %s', asset)

        elif action == 'rejectMilk':
            self._transfer = asset
            LOGGER.debug('rejectMilk action: %s', asset)

        else:
            raise InvalidTransaction('Unhandled action: {}'.format(action))

        self._action = action
        self._owner = owner
        self._asset=asset
        

    # donor property defined below
    @property
    def donor(self):
        return self._donor

   # holder property defined below
    @property
    def holder(self):
        return self._holder

    # milk property defined below
    @property
    def milk(self):
        return self._milk

  # transfer property defined below
    @property
    def transfer(self):
        return self._transfer

    @property
    def action(self):
        return self._action

    @property
    def owner(self):
        return self._owner
    @property
    def asset(self):
        return self._asset
