# Copyright 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------------

import hashlib
import json
import logging


LOGGER = logging.getLogger(__name__)


DM_NAMESPACE = hashlib.sha512(
    'donormilk'.encode('utf-8')).hexdigest()[0:6]

DM_DONOR = hashlib.sha512('111DONOR'.encode('utf-8')).hexdigest()[0:6]
DM_HOLDER = hashlib.sha512('222HOLDER'.encode('utf-8')).hexdigest()[0:6]
DM_MILK = hashlib.sha512('000MILK'.encode('utf-8')).hexdigest()[0:6]
DM_TRANSFER = hashlib.sha512('333TRANSFER'.encode('utf-8')).hexdigest()[0:6]

holderTypeDict = {
    "milkBank": "01",
    "baby": "02"
}


def _get_address(key, length):
    return hashlib.sha512(key.encode('utf-8')).hexdigest()[:length]


def _get_donor_address(donorId):
    return DM_NAMESPACE + DM_DONOR + _get_address(donorId, 58)


def _get_holder_address(holderId, holderType):
    return DM_NAMESPACE + DM_HOLDER + holderTypeDict[holderType] + _get_address(holderId, 56)


def _get_milk_address(milkBarCode):
    LOGGER.debug('milkaddress: %s',
                 DM_NAMESPACE + DM_MILK + _get_address(milkBarCode, 58),
                 )
    return DM_NAMESPACE + DM_MILK + _get_address(milkBarCode, 58)


def _get_transfer_address(holderId, milkBarCode):
    return DM_NAMESPACE + DM_TRANSFER + _get_address(holderId, 6)+_get_address(milkBarCode, 52)


def _deserialize(data):
    return json.loads(data.decode('utf-8'))


def _serialize(data):
    return json.dumps(data).encode('utf-8')


class DonorMilkState(object):

    TIMEOUT = 3

    def __init__(self, context):
        self._context = context

    # donor get function
    def get_donor(self, donor):
        # donorJson = _deserialize(donor)
        LOGGER.debug("get_donor:%s", donor)
        donorId = donor.get('donorId')
        LOGGER.debug('Donnor Id: %s',
                     donorId,
                     )
        return self._get_state(_get_donor_address(donorId))

    # donor set function
    def set_donor(self, donor, owner):
        address = _get_donor_address(donor.get('donorId'))
        donor_serial = _serialize(donor)
        LOGGER.debug('inside set_donor called, serialdonor: %s',
                     donor_serial)

        return self._context.set_state(
            {address: donor_serial}, timeout=self.TIMEOUT)

    # holder get function
    def get_holder(self, holder):
        # donorJson = _deserialize(donor)
        holderId = holder.get('holderId')
        holderType = holder.get('holderType')
        LOGGER.debug('Holder Id: %s\n Holder Type: %s',
                     holderId,
                     holderType,
                     )
        return self._get_state(_get_holder_address(holderId, holderType))

    # holder set function
    def set_holder(self, holder, owner):
        address = _get_holder_address(holder.get(
            'holderId'), holder.get('holderType'))
        holder_serial = _serialize(holder)
        return self._context.set_state(
            {address: holder_serial}, timeout=self.TIMEOUT)

   # milk get function
    def get_milk(self, milkBarCode):
        # donorJson = _deserialize(donor)
       #milkBarCode = milk.get('milkBarCode')
        LOGGER.debug('milkBarCode: %s',
                     milkBarCode,
                     )
        return self._get_state(_get_milk_address(milkBarCode))

    # milk set function
    def set_milk(self, milk, owner):
        LOGGER.debug('setmilk called',
                     )
        address = _get_milk_address(milk.get('milkBarCode'))
        LOGGER.debug('inside setmilk called, milkaddrs: %s',
                     address)
        milk_serial = _serialize({
            "milk": milk,
            "owner": owner})
        LOGGER.debug('inside setmilk called, serialmilk: %s',
                     milk_serial)
        return self._context.set_state(
            {address: milk_serial}, timeout=self.TIMEOUT)

    # transfer set function
    def set_transfer(self, transfer, owner):
        LOGGER.debug('settransfer called',
        )
        address = _get_transfer_address(
            transfer.get('holderId'), transfer.get('milkBarCode'))
        transfer_serial = _serialize(transfer)
        return self._context.set_state(
            {address: transfer_serial}, timeout=self.TIMEOUT)

   # transfer get function
    def get_transfer(self, transfer):
        LOGGER.debug('gettransfer called',
                     )
        return self._get_state(_get_transfer_address(
            transfer.get('holderId'), transfer.get('milkBarCode')))

    def delete_transfer(self, transfer):
        return self._context.delete_state(
            [_get_transfer_address(transfer.get('holderId'),transfer.get('milkBarCode'))],
            timeout=self.TIMEOUT)


    def _get_state(self, address):
        state_entries = self._context.get_state(
            [address], timeout=self.TIMEOUT)
        if state_entries:
            entry = _deserialize(data=state_entries[0].data)
        else:
            entry = None
        return entry
