import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../app/dashboard/dashboard.component';
import { DonorregisterComponent } from './donorregister/donorregister.component';
import { DonorloginComponent} from './donorlogin/donorlogin.component';
import {DonorhomeComponent} from './donorhome/donorhome.component';
import { DonordashboardComponent} from './donordashboard/donordashboard.component';
import { DonordetailsComponent} from './donordetails/donordetails.component';
import { HolderregisterComponent } from './holderregister/holderregister.component';
import { HolderloginComponent } from './holderlogin/holderlogin.component';
import {HolderhomeComponent} from './holderhome/holderhome.component';
import { HolderdashboardComponent } from './holderdashboard/holderdashboard.component';
import { HolderdetailsComponent } from './holderdetails/holderdetails.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },

  {
    path: 'donor',
    component: DonorhomeComponent,
    children: [
      {
        path: 'donorregister',
        component: DonorregisterComponent
      },
      {
        path: 'donorlogin',
        component: DonorloginComponent
      },  
    ]
  },
  {
    path: 'donordashboard',
    component: DonordashboardComponent,
    children: [
      {
        path: 'donordetails',
        component: DonordetailsComponent
      }
      
    ]
  },

  {
    path: 'consumer',
    component: HolderhomeComponent,
    children: [
      {
        path: 'consumerregister',
        component: HolderregisterComponent
      },
      {
        path: 'consumerlogin',
        component: HolderloginComponent
      },
    ]
  },
  {
    path: 'consumerdashboard',
    component: HolderdashboardComponent,
    children: [
      {
        path: 'consumerdetails',
        component: HolderdetailsComponent
      }

    ]
  },  
]

@NgModule({
  imports: [RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule],
  // CommonModule

  declarations: []
})

export class AppRoutingModule { }
