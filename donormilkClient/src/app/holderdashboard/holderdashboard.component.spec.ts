import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderdashboardComponent } from './holderdashboard.component';

describe('HolderdashboardComponent', () => {
  let component: HolderdashboardComponent;
  let fixture: ComponentFixture<HolderdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
