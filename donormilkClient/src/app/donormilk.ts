export interface Donormilk {
}

export interface Donor{
    donorName: string;
    donorId: string;
    donorPassword:string
    donorAge: number
    fitnessReport: string
    physicianApproval: string
}

export interface DonorLogin {
    username: string;
    password: string;
}

export interface HolderLogin {
    holderId: string;
    password: string;
    holderType: string;
}

export interface Holder {
    holderName: string;
    holderId: string;
    holderPassword: string
    holderType: string
}
export interface Milk {
    milkBarCode: string;
    quantity: number;
    donorId: string;
    holderId: string;

}

export interface Transfer {
    milkBarCode: string;
    holderId: string;
}





