import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, fromEvent } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SubscriptionsService {

  private socket;
  private SOCKET_URL = 'http://localhost:3000';
  private blockCommitsObserver;
  private donormilkActionObserver;

  constructor() {
    this.socket = io(this.SOCKET_URL);
    this.blockCommitsObserver = fromEvent(this.socket, 'block-commit');
    this.donormilkActionObserver = fromEvent(this.socket, 'donate-milk');
   }

  public GetBlockCommitSubscription(): Observable<any> {
    return this.blockCommitsObserver;
  }

  public GetDonateMilkActionSubscription(): Observable<any> {
     return this.donormilkActionObserver;
   }
}
