import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { DonorregisterComponent } from './donorregister/donorregister.component';
import { DonorloginComponent } from './donorlogin/donorlogin.component';
import { DonorhomeComponent } from './donorhome/donorhome.component';
import { DonordetailsComponent } from './donordetails/donordetails.component';
import { DonordashboardComponent } from './donordashboard/donordashboard.component';
import { HolderregisterComponent } from './holderregister/holderregister.component';
import { HolderloginComponent } from './holderlogin/holderlogin.component';
import { HolderhomeComponent } from './holderhome/holderhome.component';
import { HolderdashboardComponent } from './holderdashboard/holderdashboard.component';
import { HolderdetailsComponent } from './holderdetails/holderdetails.component';
import { EventsSidebarComponent } from './events-sidebar/events-sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DonorregisterComponent,
    DonorloginComponent,
    DonorhomeComponent,
    DonordetailsComponent,
    DonordashboardComponent,
    HolderregisterComponent,
    HolderloginComponent,
    HolderhomeComponent,
    HolderdashboardComponent,
    HolderdetailsComponent,
    EventsSidebarComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
