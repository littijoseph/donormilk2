import { Component, OnInit } from '@angular/core';
import { SawtoothService } from '../sawtooth.service';
import { Router } from '@angular/router';
import { Donor } from '../donormilk';

@Component({
  selector: 'app-donorregister',
  templateUrl: './donorregister.component.html',
  styles: []
})
export class DonorregisterComponent implements OnInit {

  public errorMsg: any;
  public donorFullName: string = '';
  public donorID: string = '';
  public donorPassword: string = '';
  public donorAge: number = 0;
  public fitnessReport: string = '';
  public physicianApproval: string = '';

  constructor(private router: Router, private Data: SawtoothService) { }

  ngOnInit() {
  }
  
donorregister(event){
  if (this.donorFullName.length === 0 || !this.donorFullName.trim() || this.donorID.length === 0 || !this.donorID.trim() || this.donorPassword.length === 0 || !this.donorPassword.trim()) {
    this.errorMsg = "Please enter the details"
  } else {
    var newDonor = <Donor>{};
    newDonor.donorName = this.donorFullName;
    newDonor.donorId = this.donorID;
    newDonor.donorPassword = this.Data.hash(this.donorPassword);
    newDonor.donorAge = this.donorAge;
    newDonor.fitnessReport = this.fitnessReport;
    newDonor.physicianApproval = this.physicianApproval;
    this.Data.addDonor(newDonor);
    this.router.navigate(['/donor/donorlogin']);
 
  }
}
}
