import { Component, OnInit } from '@angular/core';
import { SawtoothService } from '../sawtooth.service';
import { Router } from '@angular/router';
import { createHash } from 'crypto-browserify';
import { Buffer } from 'buffer/';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { DonorLogin } from '../donormilk';

@Component({
  selector: 'app-donorlogin',
  templateUrl: './donorlogin.component.html',
  styles: []
})
export class DonorloginComponent implements OnInit {

  public errorMsg: any;
  public myfield_control: any;
  public donorId: string = '';
  public donorPassword: string = '';
  //private address: any;   

  constructor(private router: Router, private Data: SawtoothService) { }

  ngOnInit() {
  }
  donorlogin(event) {
    //check the details
    if (this.donorId.length === 0 || !this.donorId.trim().toString() || this.donorPassword.length === 0 || !this.donorPassword.trim()) {
      this.errorMsg = "Error in User Name or Password"
    } else {
      var newLogin = <DonorLogin>{};
      newLogin.username = this.donorId;
      console.log("donorlogin called");
      newLogin.password = this.Data.hash(this.donorPassword);
      console.log("after donorpasswrd hash");

      let donorData = this.Data.getDonor(newLogin.username)
      

      donorData.then((string) => {
        const donorDataJSON = JSON.parse(string)
        console.log("donorDataJSON:"+JSON.stringify(donorDataJSON));
        let password = donorDataJSON["donorPassword"];
        console.log("password:"+password)
        if (newLogin.password === password) {
          console.log("inside if loop");
          let passPharse = this.Data.hash(this.donorId+this.donorPassword);
          this.Data.genKeyPair(passPharse.slice(0, 64));
          console.log("After genkeypair");
        
          this.Data.setCurrentDonor(donorDataJSON);
          console.log("After set current user");
          
          
          this.router.navigate(['/donordashboard/donordetails']);
        }
        this.errorMsg = "Error in User Name or Password";
      })
    }
  }

}
