import { Component, OnInit } from '@angular/core';
import { SubscriptionsService } from '../subscriptions.service';
import { RestApisService } from '../rest-apis/rest-apis.service';

@Component({
  selector: 'app-events-sidebar',
  templateUrl: './events-sidebar.component.html',
  styleUrls: ['./events-sidebar.component.css']
})
export class EventsSidebarComponent implements OnInit {

  private blockInfo = {
    blockId: String,
    blockNum: Number,
    // action: String,
    // milk: String,
    // donor: String
    message: String
  };


  private blockDetail = {
    batches: Array,
    header: Object,
    signature: String
  };


  private blockInfoChain;
  public showDetails = false;
  public blockContents;

  constructor(
    private subscriptions: SubscriptionsService,
    private restapi: RestApisService)
    {
    this.blockInfoChain = [this.blockInfo];
    this.subscriptions.GetBlockCommitSubscription()
      .subscribe((data) => this.processBlockCommitEvent(data));
    this.subscriptions.GetDonateMilkActionSubscription()
      .subscribe((data) => this.processDonateMilkActionEvent(data));
   }

  ngOnInit() {
  }

  public openFullScreen(event, blockId) {
    this.restapi.Block(undefined, undefined, blockId)
      .then(data => data.json())
      .then(blockData => {
        console.log('block data', blockData);
        this.blockContents = Object.assign({}, this.blockDetail);
        this.blockContents.batches = blockData.data.batches;
        console.log('batches', this.blockContents);
        this.showDetails = true;
      });
  }

  public processBlockCommitEvent(this, data) {
    console.log('observed event', data);
    if (data && data.blockId) {
      const newBlockInfo = Object.assign({}, this.blockInfo);
      newBlockInfo.blockId = data.blockId;
      newBlockInfo.blockNum = data.blockNum;
      newBlockInfo.message = null;
      this.blockInfoChain[data.blockNum] = newBlockInfo;
    }
  }

  public processDonateMilkActionEvent(this, data) {
    console.log('observed event', data);
    console.log("this.blockInfoChain[data.blockNum] before", this.blockInfoChain[data.blockNum]);
    if (data) {
      const newBlockInfo = Object.assign(this.blockInfoChain[data.blockNum]);
      // newBlockInfo.action = data.action;
      // newBlockInfo.milk = data.milk;
      // newBlockInfo.donor = data.donor;
      newBlockInfo.message = data.message;
    }
  }

}
