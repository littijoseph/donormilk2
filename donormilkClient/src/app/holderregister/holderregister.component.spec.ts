import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderregisterComponent } from './holderregister.component';

describe('HolderregisterComponent', () => {
  let component: HolderregisterComponent;
  let fixture: ComponentFixture<HolderregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
